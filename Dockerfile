FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y curl zsh vim git
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" || true

# setup vim
RUN apt-get install -y cmake build-essential python-dev

RUN git clone --recursive https://smshen@bitbucket.org/smshen/envconfigure.git /root/envconfigure
WORKDIR /root/envconfigure
RUN bash ./setup.sh

WORKDIR /root

