Docker Development Environment
===
Docker image for development environment, including

- zsh
    - onmyzsh
- vim
    - Youcompleteme
- git
- tmux

Usage
---
build image by docker file

    sudo docker build -t nameyouwant .

launch a container 

    sudo docker run -idt imagename

attach into container

    sudo docker exec -ti containerid zsh